##
## Does a local backup of VMs by first cloning them, then exporting them as an OVA.
## Clones will be deleted after the OVAs are backed up.
## Local Backups will be deleted based on your defined retention policy.
## Requires PowerCLI.
##
##									https://gitlab.com/vNic7/VM-Local-Backup.git
##									jbonds <virthurt.com>


## Define vCenter Connectivity
$Username = "administrator"
$Password = "7ujmMJU&"
$vcs = "jbvcsa01.blackmar.local"

Get-Module -ListAvailable | Where {$_.Name -match "VMWare"} | Import-Module
Connect-VIServer -User $Username -Password $Password -Server $vcs

$VMs = Get-Cluster | ?{$_.Name -notmatch "SANCluster01"} | Get-VM		## Use this if you want to exclude a cluster.
## $VMs = Get-Cluster | Get-VM												## Comment this if you use the option above.
$TargetDatastore = "vsanDatastore"										## Datastore where you want to temporarily clone the VMs.
$BakFolder = "VM-Backups"												## Folder where you want to temporarily clone the VMs.
$DestinationDir = "G:\VM-Backups\"										## Local Directory Backup where OVAs will be stored.
$Cloned = @()


Foreach ($i in $VMs)
{
	New-VM -Name "$($i.Name)-Clone" -VM $i.Name -Datastore $targetDatastore -Location $BakFolder -DiskStorageFormat Thin -VMHost $i.vmhost.name -Confirm:$false
	Start-Sleep 30;
	$Cloned += Get-VM "$($i.Name)-Clone"
}
Foreach ($c in $Cloned)
{
	$dateTime = Get-Date -Format MM.dd.yyyy-hhmmss
	$c | Export-Vapp -Destination $DestinationDir -Name "$($c.Name)_$($dateTime)" -Format OVA -Confirm:$false
}
Start-Sleep 30;
get-folder $BakFolder | get-vm | Remove-VM -DeletePermanently -Confirm:$false
Start-Sleep 30;
Disconnect-VIServer -Confirm:$false

######
##
## Uncomment Below if you want to apply a retention policy.
## Specify the number of days you want to keep your backups.
##


$RetentionDays = 5
$Retention = (Get-Date).AddDays(-$RetentionDays)
$Purge = gci $DestinationDir | ?{$_.LastWriteTime -lt $Retention}
$Purge | %{Remove-Item "$($DestinationDir)$($_)"}                                       


